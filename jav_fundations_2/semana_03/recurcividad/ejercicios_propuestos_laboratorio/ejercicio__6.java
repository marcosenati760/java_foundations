class ejercici__6{ public static void calcularResultados() {
    try {
        FileReader fr = new FileReader("datos_generados.txt");
        BufferedReader br = new BufferedReader(fr);
        int[] datos = new int;
        String linea;
        int i = 0;
        while ((linea = br.readLine()) != null) {
            int dato = Integer.parseInt(linea);
            datos[i] = dato;
            i++;
        }
        br.close();
        double suma = 0;
        int maximo = datos;
        int minimo = datos;
        int moda = 0;
        int frecuenciaModa = 0;
        double desviacion = 0;
        for (int j = 0; j < datos.length; j++) {
            suma += datos[j];
            if (datos[j] > maximo) {
                maximo = datos[j];
            }
            if (datos[j] < minimo) {
                minimo = datos[j];
            }
            int frecuencia = 0;
            for (int k = 0; k < datos.length; k++) {
                if (datos[k] == datos[j]) {
                    frecuencia++;
                }
            }
            if (frecuencia > frecuenciaModa) {
                moda = datos[j];
                frecuenciaModa = frecuencia;
            }
            double diferencia = datos[j] - (suma / datos.length);
            desviacion += Math.pow(diferencia, 2);
        }
        double promedio = suma / datos.length;
        desviacion = Math.sqrt(desviacion / datos.length);
        FileWriter fw = new FileWriter("resultados.txt");
        fw.write("Valor: " + suma + "\n");
        fw.write("Máximo: " + maximo + "\n");
        fw.write("Mínimo: " + minimo + "\n");
        fw.write("Promedio: " + promedio + "\n");
        fw.write("Moda: " + moda + "\n");
        fw.write("Desviación estándar: " + desviacion + "\n");
        fw.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
}