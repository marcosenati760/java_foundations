public class ejercici__7{
    public static void ordenar(int[] datos) {
        quicksort(datos, 0, datos.length - 1);
    }

    private static void quicksort(int[] datos, int izquierda, int derecha) {
        if (izquierda < derecha) {
            int indicePivote = particion(datos, izquierda, derecha);
            quicksort(datos, izquierda, indicePivote - 1);
            quicksort(datos, indicePivote + 1, derecha);
        }
    }

    private static int particion(int[] datos, int izquierda, int derecha) {
        int pivote = datos[derecha];
        int i = izquierda - 1;
        for (int j = izquierda; j < derecha; j++) {
            if (datos[j] < pivote) {
                i++;
                int temp = datos[i];
                datos[i] = datos[j];
                datos[j] = temp;
            }
        }
        int temp = datos[i + 1];
        datos[i + 1] = datos[derecha];
        datos[derecha] = temp;
        return i + 1;
    }
}