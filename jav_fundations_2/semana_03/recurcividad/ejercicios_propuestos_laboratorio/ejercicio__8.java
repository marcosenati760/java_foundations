public class ejercici__8 {
    private String nombreCompleto;
    private String grupoPersonas;

    public Persona(String nombreCompleto, String grupoPersonas) {
        this.nombreCompleto = nombreCompleto;
        this.grupoPersonas = grupoPersonas;
    }
    public class Trabajador extends Persona {
    public Trabajador(String nombreCompleto, String grupoPersonas) {
        super(nombreCompleto, grupoPersonas);
    }
    public static void leerDatos() {
    try {
        FileInputStream file = new FileInputStream(new File("clientes.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next(); // saltar la primera fila (encabezados)
        ArrayList<Trabajador> trabajadores = new ArrayList<>();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            String nombreCompleto = row.getCell(0).getStringCellValue();
            String grupoPersonas = row.getCell(1).getStringCellValue();
            Trabajador trabajador = new Trabajador(nombreCompleto, grupoPersonas);
            trabajadores.add(trabajador);
        }
        file.close();
        workbook.close();
        Trabajador[] arregloTrabajadores = trabajadores.toArray(new Trabajador[trabajadores.size()]);
        ordenarDatos(arregloTrabajadores);
    } catch (IOException e) {
        e.printStackTrace();
    }
}
}
    }