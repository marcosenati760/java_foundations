public class ejercici__2 {
    public static int calcular(int[] numeros) {
        int resultado = numeros;
        for (int i = 1; i < numeros.length; i++) {
            resultado = calcular(resultado, numeros[i]);
        }
        return resultado;
    }

    public static int calcular(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcular(b, a % b);
        }
    }
}
public class Main {
    public static void main(String[] args) {
        int[] numeros = {12, 18, 24}; // los números para los que queremos calcular el MCD
        int resultado = MaximoComunDivisor.calcular(numeros);
        System.out.println("El MCD es: " + resultado);
    }
}