public class ejercici__3{
    public static int calcular(int[] numeros) {
        int resultado = numeros;
        for (int i = 1; i < numeros.length; i++) {
            resultado = (resultado * numeros[i]) / mcd(resultado, numeros[i]);
        }
        return resultado;
    }

    public static int mcd(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return mcd(b, a % b);
        }
    }
}