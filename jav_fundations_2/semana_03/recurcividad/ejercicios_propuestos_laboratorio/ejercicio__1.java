public class ejercicio__1 {
    public static int calcular(int n) {
        if (n <= 1) {
            return n;
        } else {
            return calcular(n-1) + calcular(n-2);
        }
    }
}