import java.util.Scanner;
import java.lang.Math;

public class ejerciciosecuencial5operaciones {
    public static void main(String[] args) {
        Scanner operaciones = new Scanner(System.in);
        Scanner input = new Scanner(System.in);
        System.out.print("ingresa primer numero: ");

        double num1 = input.nextDouble();
        System.out.print("ingresa segundo numero: ");
        double num2 = input.nextDouble();
        System.out.println("elige operator (+,-,*,/,^): ");
        char operator = input.next().charAt(0);
        double result;
        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            case '^':
                result = Math.pow(num1, num2);
                break;
            default:
                System.out.println("Operación no válida");
                return;
        }
        System.out.println("El resultado es: " + result);
    }

}
