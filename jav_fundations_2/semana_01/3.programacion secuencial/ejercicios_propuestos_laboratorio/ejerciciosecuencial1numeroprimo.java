
import java.util.Scanner;

public class ejerciciosecuencial1numeroprimo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("--------------------------------------------------------\n");
        System.out.print("Ingrese un número entero para saber si es primo o no: ");
        System.out.print("\n--------------------------------------------------------\n");
        int num = sc.nextInt();
        boolean esPrimo = true;
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                esPrimo = false;
                break;
            }
        }
        if (esPrimo) {
            System.out.print("\n-------------------------------------------------\n");
            System.out.println(num + " el numero que mensiona es un número primo");
            System.out.print("-------------------------------------------------\n");
        } else {
            System.out.print("\n--------------------------------------------------\n");
            System.out.println(num + " el numero que menciona no es un número primo");
            System.out.print("\n--------------------------------------------------\n");
        }
    }

}
