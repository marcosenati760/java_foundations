import java.util.Scanner;

public class ejerciciosecuencial6grados {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese la temperatura en grados : ");
        double celsius = input.nextDouble();

        double fahrenheit = (9 * celsius / 5) + 32;

        System.out.println(celsius + " grados CELsius a grados Fahrenheit es: " + fahrenheit);
    }
}