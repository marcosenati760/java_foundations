import java.nio.channels.ScatteringByteChannel;
import java.util.Scanner;

public class ejerciciosecuencial2elipse {
    public static void main(String[] args) {
        float pi = (float) 3.1416;
        Scanner leer_datos = new Scanner(System.in);
        double a, b, area, perimetro;
        System.out.println("Ingrese a:");
        a = leer_datos.nextDouble();
        System.out.println("ingrese b:");
        b = leer_datos.nextDouble();
        area = pi * a * b;
        perimetro = pi * (a + b);
        System.out.println("el area de elipse es:" + area);
        System.out.println("el perimetro de elipse es:" + perimetro);

    }
}