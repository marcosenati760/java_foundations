import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ejerciciosalgoritmos11casa {
    public static void main(String[] args) {
        int contador = 0;
        try {
            File archivo = new File("archivo.txt");
            Scanner scanner = new Scanner(archivo);
            while (scanner.hasNextLine()) {
                String linea = scanner.nextLine();
                String[] palabras = linea.split(" ");
                for (String palabra : palabras) {
                    if (tieneLetrasDiferentes(palabra.toLowerCase())) {
                        contador++;
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        }
        System.out.println("Hay " + contador + " palabras que tienen todas las letras diferentes.");
    }

    public static boolean tieneLetrasDiferentes(String palabra) {
        for (int i = 0; i < palabra.length(); i++) {
            char letra = palabra.charAt(i);
            for (int j = i + 1; j < palabra.length(); j++) {
                if (palabra.charAt(j) == letra) {
                    return false;
                }
            }
        }
        return true;
    }
}