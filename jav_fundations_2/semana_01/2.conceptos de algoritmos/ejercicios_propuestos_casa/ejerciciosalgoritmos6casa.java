public class ejerciciosalgoritmos6casa {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (esIgualASumaFactoriales(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador
                + " números enteros que tienen la propiedad de ser iguales a la suma de los dígitos factoriales de su descomposición en dígitos.");
    }

    public static boolean esIgualASumaFactoriales(int numero) {
        int suma = 0;
        int n = numero;
        while (n != 0) {
            int digito = n % 10;
            suma += factorial(digito);
            n /= 10;
        }
        return numero == suma;
    }

    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}