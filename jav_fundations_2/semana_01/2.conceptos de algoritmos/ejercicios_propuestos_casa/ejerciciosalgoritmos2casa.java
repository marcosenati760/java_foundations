public class ejerciciosalgoritmos2casa {
    public static void main(String[] args) {
        int numero = 1;
        while (true) {
            boolean divisible = true;
            for (int i = 1; i <= 20; i++) {
                if (numero % i != 0) {
                    divisible = false;
                    break;
                }
            }
            if (divisible) {
                System.out.println("El número " + numero + " es divisible por todos los números de 1 a 20.");
                break;
            }
            numero++;
        }
    }
}