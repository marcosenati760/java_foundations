import java.util.ArrayList;
import java.util.Scanner;

public class ejerciciosalgoritmos20casa {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese una lista de palabras separadas por comas: ");
        String input = sc.nextLine();
        String[] words = input.split(",");
        int count = 0;
        for (String word : words) {
            int consecutiveVowels = word.toLowerCase().split("[aeiou]{2,}").length - 1;
            if (consecutiveVowels > 2) {
                count++;
            }
        }
        System.out.println("Hay " + count + " palabras con más de dos vocales consecutivas.");
    }
}