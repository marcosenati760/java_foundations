public class ejerciciosalgoritmos18casa {
    public static void main(String[] args) {
        int suma = 0;
        for (int i = 1; i <= 100; i++) {
            suma += sumarDigitos(i);
        }
        int numero = 1;
        while (true) {
            if (numero % suma == 0) {
                System.out.println("El número " + numero
                        + " es divisible por la suma de los dígitos de todos los números de 1 a 100.");
                break;
            }
            numero++;
        }
    }

    public static int sumarDigitos(int numero) {
        int suma = 0;
        while (numero != 0) {
            suma += numero % 10;
            numero /= 10;
        }
        return suma;
    }
}