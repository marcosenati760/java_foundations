import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ejerciciosalgoritmos12casa {
    public static void main(String[] args) {
        int contador = 0;
        try {
            File archivo = new File("archivo.txt");
            Scanner scanner = new Scanner(archivo);
            while (scanner.hasNextLine()) {
                String linea = scanner.nextLine();
                String[] palabras = linea.split(" ");
                for (String palabra : palabras) {
                    if (tieneLetraRepetida(palabra.toLowerCase())) {
                        contador++;
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        }
        System.out.println("Hay " + contador + " palabras que tienen la misma letra repetida tres veces consecutivas.");
    }

    public static boolean tieneLetraRepetida(String palabra) {
        for (int i = 0; i < palabra.length() - 2; i++) {
            if (palabra.charAt(i) == palabra.charAt(i + 1) && palabra.charAt(i) == palabra.charAt(i + 2)) {
                return true;
            }
        }
        return false;
    }
}