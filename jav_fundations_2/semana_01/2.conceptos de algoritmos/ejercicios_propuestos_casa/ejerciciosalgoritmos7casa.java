public class ejerciciosalgoritmos7casa {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (esNumeroHarshad(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números enteros que son números de Harshad.");
    }

    public static boolean esNumeroHarshad(int numero) {
        int sumaDigitos = 0;
        int numeroOriginal = numero;
        while (numero != 0) {
            int digito = numero % 10;
            sumaDigitos += digito;
            numero /= 10;
        }
        return numeroOriginal % sumaDigitos == 0;
    }
}