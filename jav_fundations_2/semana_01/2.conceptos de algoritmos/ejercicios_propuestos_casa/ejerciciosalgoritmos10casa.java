public class ejerciciosalgoritmos10casa {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (esNumeroSmith(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números enteros que son números de Smith.");
    }

    public static boolean esNumeroSmith(int numero) {
        int sumaDigitos = sumarDigitos(numero);
        int sumaFactores = sumarDigitosFactores(numero);
        return sumaDigitos == sumaFactores;
    }

    public static int sumarDigitos(int numero) {
        int suma = 0;
        while (numero != 0) {
            suma += numero % 10;
            numero /= 10;
        }
        return suma;
    }

    public static int sumarDigitosFactores(int numero) {
        int suma = 0;
        for (int i = 2; i <= numero; i++) {
            while (numero % i == 0) {
                suma += sumarDigitos(i);
                numero /= i;
            }
        }
        return suma;
    }
}