import java.util.Scanner;

public class ejercicio4algoritmos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int lado1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int lado2 = scanner.nextInt();
        System.out.print("Ingrese el tercer número entero: ");
        int lado3 = scanner.nextInt();
        if (lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1) {
            System.out.println("Los números pueden formar un triángulo válido");
        } else {
            System.out.println("Los números no pueden formar un triángulo válido");
        }
        scanner.close();
    }
}