import java.util.HashMap;

public class ejercicio2casa {
    public static void main(String[] args) {
        HashMap<String, Integer> nombresEdades = new HashMap<String, Integer>();
        nombresEdades.put("Juan", 25);
        nombresEdades.put("María", 30);
        nombresEdades.put("Pedro", 40);
        nombresEdades.put("Ana", 35);
        System.out.println("La lista de nombres y edades es: " + nombresEdades);
    }
}