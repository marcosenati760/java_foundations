import java.math.BigDecimal;

public class ejercicio14casa {
    public static void main(String[] args) {
        // Crear dos objetos BigDecimal con los valores deseados
        BigDecimal numero1 = new BigDecimal("8.5");
        BigDecimal numero2 = new BigDecimal("58.2");

        // Realizar algunas operaciones matemáticas con los objetos BigDecimal
        BigDecimal suma = numero1.add(numero2);
        BigDecimal resta = numero1.subtract(numero2);
        BigDecimal multiplicacion = numero1.multiply(numero2);
        BigDecimal division = numero1.divide(numero2, 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal potencia = numero1.pow(2);
        BigDecimal raizCuadrada = BigDecimal.valueOf(Math.sqrt(numero2.doubleValue()));

        // Imprimir los resultados
        System.out.println("La suma de " + numero1 + " y " + numero2 + " es: " + suma);
        System.out.println("La resta de " + numero1 + " y " + numero2 + " es: " + resta);
        System.out.println("La multiplicación de " + numero1 + " y " + numero2 + " es: " + multiplicacion);
        System.out.println("La división de " + numero1 + " y " + numero2 + " es: " + division);
        System.out.println("El cuadrado de " + numero1 + " es: " + potencia);
        System.out.println("La raíz cuadrada de " + numero2 + " es: " + raizCuadrada);
    }
}