import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio11casa {
    public static void main(String[] args) {
        String patron = "\\d+";
        Pattern pattern = Pattern.compile(patron);
        String texto = "La edad de Juan es 25";
        Matcher matcher = pattern.matcher(texto);
        if (matcher.find()) {
            System.out.println("Se encontró un número en el texto: " + matcher.group());
        } else {
            System.out.println("No se encontró un número en el texto.");
        }
    }
}