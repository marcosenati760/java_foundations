import java.io.*;

public class ejercicio18casa {
    public static void main(String[] args) {
        // Crear un objeto InputStream para leer datos de entrada
        InputStream datosEntrada = System.in;

        // Leer los datos de entrada
        try {
            byte[] buffer = new byte;
            int longitud = datosEntrada.read(buffer);
            String datos = new String(buffer, 0, longitud);
            System.out.println("Los datos de entrada son: " + datos);
        } catch (IOException e) {
            System.out.println("Error al leer los datos de entrada");
        }
    }
}