import java.text.SimpleDateFormat;
import java.util.Date;

public class ejercicio13casa {
    public static void main(String[] args) {
        // Crear un objeto SimpleDateFormat con el formato deseado
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

        // Obtener la fecha y hora actual
        Date fechaHoraActual = new Date();

        // Formatear la fecha y hora actual utilizando el objeto SimpleDateFormat
        String fechaHoraFormateada = formato.format(fechaHoraActual);

        // Imprimir la fecha y hora formateada
        System.out.println("La fecha y hora actual es: " + fechaHoraFormateada);
    }
}