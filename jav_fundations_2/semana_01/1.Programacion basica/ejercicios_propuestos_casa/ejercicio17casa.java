import java.util.Locale;

public class ejercicio17casa {
    public static void main(String[] args) {
        // Crear un objeto Locale con la configuración regional deseada
        Locale configuracionRegional = new Locale("es", "ES");

        // Imprimir la información de la configuración regional
        System.out.println("El idioma es: " + configuracionRegional.getLanguage());
        System.out.println("El país es: " + configuracionRegional.getCountry());
    }
}