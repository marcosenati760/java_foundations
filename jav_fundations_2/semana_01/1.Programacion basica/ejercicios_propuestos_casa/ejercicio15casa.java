import java.math.BigInteger;

public class ejercicio15casa {
    public static void main(String[] args) {
        // Crear dos objetos BigInteger con los valores deseados
        BigInteger numero1 = new BigInteger("12345");
        BigInteger numero2 = new BigInteger("98765");

        // Realizar algunas operaciones matemáticas con los objetos BigInteger
        BigInteger suma = numero1.add(numero2);
        BigInteger resta = numero1.subtract(numero2);
        BigInteger multiplicacion = numero1.multiply(numero2);
        BigInteger division = numero1.divide(numero2);
        BigInteger potencia = numero1.pow(2);

        // Imprimir los resultados
        System.out.println("La suma de " + numero1 + " y " + numero2 + " es: " + suma);
        System.out.println("La resta de " + numero1 + " y " + numero2 + " es: " + resta);
        System.out.println("La multiplicación de " + numero1 + " y " + numero2 + " es: " + multiplicacion);
        System.out.println("La división de " + numero1 + " y " + numero2 + " es: " + division);
        System.out.println("El cuadrado de " + numero1 + " es: " + potencia);
    }
}