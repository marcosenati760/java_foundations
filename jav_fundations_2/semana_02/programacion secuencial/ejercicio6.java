public class ejercicio6 {
    public static double calcularVarianza(int[] arreglo) {
        double media = calcularMedia(arreglo);
        double sumaCuadrados = 0;
        for (int i = 0; i < arreglo.length; i++) {
            sumaCuadrados += Math.pow(arreglo[i] - media, 2);
        }
        double varianza = sumaCuadrados / arreglo.length;
        return varianza;
    }
    
    public static double calcularMedia(int[] arreglo) {
        double suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        double media = suma / arreglo.length;
        return media;
    }
}