public class ejercicio5 {
    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 4, 5};
        int elemento = 3;
        int posicion = BusquedaArreglo.buscarElemento(arreglo, elemento);
        if (posicion != -1) {
            System.out.println("El elemento " + elemento + " está en la posición " + posicion);
        } else {
            System.out.println("El elemento " + elemento + " no está en el arreglo");
        }
    }
}