import java.math.BigInteger;

public class ejercicio3 {
    public static void main(String[] args) {
        int n = 50;
        BigInteger[] factoriales = new BigInteger[n];
        
        for (int i = 0; i < n; i++) {
            factoriales[i] = calcularFactorial(i + 1);
        }
        
        for (int i = 0; i < n; i++) {
            System.out.println((i + 1) + "! = " + factoriales[i]);
        }
    }
    
    public static BigInteger calcularFactorial(int n) {
        BigInteger factorial = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}