public class ejercicio {
    public static void main(String[] args) {
        String[][] estudiantes = {
            {"Juan", "20"},
            {"María", "22"},
            {"Pedro", "19"},
            {"Ana", "21"},
            {"Luis", "20"}
        };
        
        int sumaEdades = 0;
        for (int i = 0; i < estudiantes.length; i++) {
            sumaEdades += Integer.parseInt(estudiantes[i][1]);
        }
        
        double promedioEdades = (double) sumaEdades / estudiantes.length;
        
        System.out.println("El promedio de edades de los estudiantes es: " + promedioEdades);
    }
}