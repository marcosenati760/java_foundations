public class ejercicoi4{
    public static int productoArreglo(int[] arreglo) {
        int producto = 1;
        for (int i = 0; i < arreglo.length; i++) {
            producto *= arreglo[i];
        }
        return producto;
    }
}