import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entre 1 y 3999: ");
        int numero = sc.nextInt();
        if (numero < 1 || numero > 3999) {
            System.out.println("El número ingresado no es válido");
            return;
        }
        String[] unidades = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        String[] decenas = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String[] centenas = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String[] millares = {"", "M", "MM", "MMM"};
        String romano = millares[numero / 1000] + centenas[(numero % 1000) / 100] + decenas[(numero % 100) / 10] + unidades[numero % 10];
        System.out.println("El número " + numero + " en números romanos es: " + romano);
    }
}