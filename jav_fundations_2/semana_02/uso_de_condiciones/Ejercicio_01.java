public class Ejercicio_01 {

    public static void main(String[] args) {
        cuadrado(7);
    }

    public static void cuadrado(int n) {

        for (int i = 0; i < n; i++) {

            for (int j = 0; j < n; j++) {

                if ((i % 1 == 0 && j % 1 == 0) || (i % 2 == 0 && j % 2 == 0)) {
                    System.out.print(" * ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

}
