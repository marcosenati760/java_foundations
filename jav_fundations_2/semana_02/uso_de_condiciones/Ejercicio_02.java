
public class Ejercicio_02 {
    public static void main(String[] args) {
        int n = 7;
        int jh = n / 2;

        for (int i = 0; i < n; i++) {

            int numEspacios = Math.abs(jh - i);
            int numAsteriscos = n - 2 * numEspacios;

            for (int j = 0; j < numEspacios; j++) {
                System.out.print(" ");
            }

            for (int j = 0; j < numAsteriscos; j++) {
                System.out.print("*");
            }

            System.out.println();
        }
    }
}
