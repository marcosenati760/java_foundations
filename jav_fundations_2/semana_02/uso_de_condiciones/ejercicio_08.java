import java.util.Scanner;

public class ejercicio_08 {
    public static void main(String[] args) {
        int num, factorial = 1, i = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce un número: ");
        num = sc.nextInt();
        while (i <= num) {
            factorial *= i;
            i++;
            System.out.println("El factorial de " + num + " es " + factorial);
        }
    }
}
