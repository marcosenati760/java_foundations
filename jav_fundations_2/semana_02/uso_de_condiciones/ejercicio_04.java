import java.util.Scanner;

public class ejercicio_04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un valor: ");
        int n = sc.nextInt();
        sc.close();

        System.out.print("Los números primos desde 2 hasta " + n + " son: ");
        for (int i = 2; i <= n; i++) {
            boolean esPrimo = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo) {
                System.out.print(i + " ");
            }
        }
    }
}